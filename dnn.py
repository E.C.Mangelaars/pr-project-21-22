import time
import argparse

import tensorflow as tf
from tensorflow import keras

pre_training_timer = 0

LEARNING_RATE_BASE = 0.8
LEARNING_RATE_DECAY = 0.99
REGULARAZTION_RATE = 0.0001
TRAINING_STEPS = 600
MOVING_AVERAGE_DECAY = 0.99


def main(truncate_dataset=False):
    tf.get_logger().setLevel('INFO')
    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
    # tf.config.set_visible_devices([], 'GPU')

    if truncate_dataset:
        print('--truncate was specified, so only using 10 rows from dataset')

    model = dnn_model()

    scores = []
    for i in range(1, 6):
        print('\n\n' + str(i))
        train = str(i) + '/train.Matrix'
        test = str(i) + '/test.Matrix'

        x_train, y_train = read_case(train)
        x_test, y_test = read_case(test)

        if truncate_dataset:
            x_train = x_train[:10]
            y_train = y_train[:10]
            x_test = x_test[:10]
            y_test = y_test[:10]

        total_timer = time.perf_counter()
        score = run_dnn(model, x_train, y_train, x_test, y_test, )
        print('Total time of train/test run: {:.1f} seconds'.format(time.perf_counter() - total_timer))

        print(f'Test loss: {score[0]} / Test accuracy: {score[1]}')

        scores.append(score[1])

    print('final accuracy: {}'.format(sum(scores) / len(scores)))


def dnn_model():
    model = keras.Sequential([
        keras.Input(shape=10000),
        keras.layers.Dense(units=256, activation=tf.nn.relu, kernel_regularizer='l2'),
        keras.layers.Dense(units=256, activation=tf.nn.relu, kernel_regularizer='l2'),
        keras.layers.Dense(units=256, activation=tf.nn.relu, kernel_regularizer='l2'),
        keras.layers.Dense(units=2)
    ])
    model.summary()

    learning_rate = tf.keras.optimizers.schedules.ExponentialDecay(
        LEARNING_RATE_BASE, TRAINING_STEPS, LEARNING_RATE_DECAY, staircase=False, name=None
    )
    model.compile(
        optimizer=keras.optimizers.SGD(learning_rate=learning_rate),
        metrics=['accuracy'],
        loss=keras.losses.CategoricalCrossentropy()
    )

    return model


def run_dnn(model, x_train, y_train, x_test, y_test):
    print('\nFitting...')
    global pre_training_timer
    pre_training_timer = time.perf_counter()
    model.fit(
        x=x_train,
        y=y_train,
        epochs=1,
        verbose=2,
        callbacks=[CustomCallback()]
    )
    score = model.evaluate(
        x_test,
        y_test,
        verbose=2,
        callbacks=[CustomCallback()]
    )
    return score


def read_case(path):
    F = open(path, 'r')

    x = []
    y = []
    for line in F:
        lst = line.strip()
        lst = lst.split('\t')
        read_list = lst[0].split(',')
        results = list(map(int, read_list))
        x.append(results)
        if lst[1] == '0,1':
            y.append([0, 1])
        else:
            y.append([1, 0])

    return x, y


class CustomCallback(keras.callbacks.Callback):
    def on_train_begin(self, logs=None):
        pre_training = time.perf_counter() - pre_training_timer
        print("    Starting training after {:.1f} seconds".format(pre_training))

    def on_train_end(self, logs=None):
        print("    Stop training")

    def on_epoch_begin(self, epoch, logs=None):
        print("    Start epoch {} of training".format(epoch))

    def on_epoch_end(self, epoch, logs=None):
        print("    End epoch {} of training".format(epoch))

    def on_test_begin(self, logs=None):
        print("    Start testing")

    def on_test_end(self, logs=None):
        print("    Stop testing")

    def on_predict_begin(self, logs=None):
        print("    Start predicting")

    def on_predict_end(self, logs=None):
        print("    Stop predicting")

    def on_train_batch_begin(self, batch, logs=None):
        print("    ...Training: start of batch {}".format(batch))

    def on_train_batch_end(self, batch, logs=None):
        print("    ...Training: end of batch {}".format(batch))

    def on_test_batch_begin(self, batch, logs=None):
        print("    ...Evaluating: start of batch {}".format(batch))

    def on_test_batch_end(self, batch, logs=None):
        print("    ...Evaluating: end of batch {}".format(batch))

    def on_predict_batch_begin(self, batch, logs=None):
        print("    ...Predicting: start of batch {}".format(batch))

    def on_predict_batch_end(self, batch, logs=None):
        print("    ...Predicting: end of batch {}".format(batch))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--truncate', default=False, action="store_true")
    arguments = parser.parse_args()

    main(truncate_dataset=arguments.truncate)
