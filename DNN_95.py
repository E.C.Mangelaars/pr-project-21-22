import tensorflow as tf
import numpy as np
import BRCA_data


class ToySequenceData(object):
    def __init__(self, data, labels ):
        self.data = data
        self.labels = labels
        self.batch_id = 0

    def next(self, batch_size):
        """ Return a batch of data. When dataset end is reached, start over.
        """
        if self.batch_id == len(self.data):
            self.batch_id = 0
        batch_data = (self.data[self.batch_id:min(self.batch_id + batch_size, len(self.data))])
        batch_labels = (self.labels[self.batch_id:min(self.batch_id + batch_size, len(self.data))])
        self.batch_id = min(self.batch_id + batch_size, len(self.data))
        return batch_data, batch_labels


def read_all(L):
    x = []
    y = []
    for l in L:
        lst = l.split()
        lst_x = lst[0].split(',')
        lst_y = lst[1].split(',')
        x.append(lst_x)
        y.append(lst_y)
    return x, y


data_test, labels_test = read_all(BRCA_data.readCase("test.Matrix"))
data_train, labels_train = read_all(BRCA_data.readCase("train.Matrix"))

trainset = ToySequenceData(data_train, labels_train)

print(np.array(data_train).shape)

n_hidden1 = 256
n_hidden2 = 256
n_hidden3 = 256
n_outputs = 2

X = tf.placeholder(tf.float32, shape=[None, 10000], name="X")
y = tf.placeholder(tf.float32, shape=[None, 2], name="y")


def neuron_layer(X, n_neurons, name, activation=None):
    with tf.name_scope(name):
        n_inputs = int(X.get_shape()[1])
        stddev = 2 / np.sqrt(n_inputs)
        init = tf.truncated_normal((n_inputs, n_neurons), stddev=stddev)
        W = tf.Variable(init, name="weights")
        b = tf.Variable(tf.zeros([n_neurons]), name="biases")
        z = tf.matmul(X, W) + b
        if activation == "relu":
            return tf.nn.relu(z)
        else:
            return z


with tf.name_scope("dnn"):
    hidden1 = tf.layers.dense(X, n_hidden1, name="hidden1",
                              activation=tf.nn.relu)
    hidden2 = tf.layers.dense(hidden1, n_hidden2, name="hidden2",
                              activation=tf.nn.relu)
    hidden3 = tf.layers.dense(hidden2, n_hidden3, name="hidden3",
                              activation=tf.nn.relu)
    logits = tf.layers.dense(hidden3, n_outputs, name="outputs")

with tf.name_scope("loss"):
    xentropy = tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=logits)
    loss = tf.reduce_mean(xentropy, name="loss")

global_step = tf.Variable(0, trainable=False)
learning_rate = tf.train.exponential_decay(0.01, global_step, decay_steps=600, decay_rate=1, staircase=True)

with tf.name_scope("train"):
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    training_op = optimizer.minimize(loss, global_step=global_step)

with tf.name_scope("eval"):
    correct = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

n_epochs = 50
batch_size = 50

init = tf.global_variables_initializer()

with tf.Session() as sess:
    init.run()
    for epoch in range(n_epochs):
        for iteration in range(1328 // batch_size):
            X_batch, y_batch = trainset.next(batch_size)
            sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
        acc_train = accuracy.eval(feed_dict={X: X_batch, y: y_batch})
        acc_test = accuracy.eval(feed_dict={X: data_test, y: labels_test})
        print(epoch, "Train accuracy:", acc_train, "Test accuracy:", acc_test)

